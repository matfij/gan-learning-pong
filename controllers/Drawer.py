import pygame


pygame.font.init()
font = pygame.font.SysFont("comicsans", 50)

clock = pygame.time.Clock()
fps = 30


def DrawAll(window, field, padsLeft, padsRight, balls):

    field.Draw(window)

    for p in padsLeft:
        p.Draw(window)

    for p in padsRight:
        p.Draw(window)

    for b in balls:
        b.Draw(window)

    # text = font.render("Score: " + str(scores), 1, (255, 255, 255))
    # window.blit(text, (25, 25))

    pygame.display.update()
    clock.tick(fps)
