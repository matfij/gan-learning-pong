from controllers import Drawer
from models.FieldModel import FieldModel
from models.BallModel import BallModel
from models.PadModel import PadModel
import pygame
import numpy


# window settings
WIN_WIDTH = 1140
WIN_HEIGHT = 568
window = pygame.display.set_mode((WIN_WIDTH, WIN_HEIGHT))


def Run(field, padsLeft, padsRight, balls, netsLeft, netsRight, fitsLeft, fitsRight):

    while True:

        # control events
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
        if (len(balls) or len(netsRight) or len(padsRight)) < 1:
            break

        try:
            # moving objects
            for b in balls:
                b.Move()
            for ind, p in enumerate(padsLeft):
                out1, out2, out3, = netsLeft[ind].activate((balls[ind].y, p.y))
                if out1 > out2 and out1 > out3:
                    p.Move(1)
                    if -100 < p.y < 1300:
                        fitsLeft[ind].fitness += 0.02
                elif out2 > out1 and out2 > out3:
                    p.Move(2)
                    if -100 < p.y < 1300:
                        fitsLeft[ind].fitness += 0.02
            for ind, p in enumerate(padsRight):
                out1, out2, out3, = netsRight[ind].activate((balls[ind].y, p.y))
                if out1 > out2 and out1 > out3:
                    p.Move(1)
                    if -100 < p.y < 1300:
                        fitsRight[ind].fitness += 0.02
                elif out2 > out1 and out2 > out3:
                    p.Move(2)
                    if -100 < p.y < 1300:
                        fitsRight[ind].fitness += 0.02

            # pad collision detection
            for ind, ball in enumerate(balls):
                if ball.Collide(padsRight[ind]):
                    ball.Bounce(0, padsRight[ind])
                if ball.Collide(padsLeft[ind]):
                    ball.Bounce(0, padsLeft[ind])

            # wall collision detection
            for ind, ball in enumerate(balls):
                if ball.y < 1 or ball.y > 555:
                    ball.Bounce(1)
            # points assignment
                if ball.x < 0 or ball.x > WIN_WIDTH:
                    balls.pop(ind)
                    netsLeft.pop(ind)
                    netsRight.pop(ind)
                    padsLeft.pop(ind)
                    padsRight.pop(ind)
                    if ball.x < 0:
                        fitsLeft[ind].fitness += 10
                    else:
                        fitsRight[ind].fitness += 10
                    fitsLeft.pop(ind)
                    fitsRight.pop(ind)

            # drawing objects
            Drawer.DrawAll(window, field, padsLeft, padsRight, balls)
        except:
            print("runtime out of range exception")
            break
