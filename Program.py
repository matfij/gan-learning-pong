from controllers import FileLoader, Engine
import neat
from models.FieldModel import FieldModel
from models.BallModel import BallModel
from models.PadModel import PadModel
import os


def main(genomes, config):

    field = FieldModel()
    balls = []
    padsLeft = []
    padsRight = []

    fitsLeft = []
    fitsRight = []
    netsLeft = []
    netsRight = []

    for ind, g in genomes:

        net = neat.nn.FeedForwardNetwork.create(g, config)
        g.fitness = 0

        if ind % 2 == 0:
            balls.append(BallModel())
            netsLeft.append(net)
            padsLeft.append(PadModel(8, 210))
            fitsLeft.append(g)
        else:
            netsRight.append(net)
            padsRight.append(PadModel(1115, 210))
            fitsRight.append(g)

    Engine.Run(field, padsLeft, padsRight, balls, netsLeft, netsRight, fitsLeft, fitsRight)


if __name__ == '__main__':

    # neat settings
    local_dir = os.path.dirname(__file__)
    config_file = FileLoader.LoadFile("neat_config.txt", local_dir)
    config = neat.config.Config(neat.DefaultGenome, neat.DefaultReproduction, neat.DefaultSpeciesSet,
                                neat.DefaultStagnation, config_file)

    pop = neat.Population(config)
    pop.add_reporter(neat.StdOutReporter(True))
    pop.run(main, 50)
