from controllers import FileLoader
import pygame


class FieldModel:

    def __init__(self):
        IMG_FILE = FileLoader.LoadFile("field.png", "img")
        self.IMG = pygame.image.load(IMG_FILE)

    def Draw(self, window):
        window.blit(self.IMG, (0, 0))
