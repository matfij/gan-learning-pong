from controllers import FileLoader
import pygame
import math
import random


class BallModel:

    def __init__(self):
        IMG_FILE = FileLoader.LoadFile("ball.png", "img")
        self.img = pygame.image.load(IMG_FILE)
        self.x = 550
        self.y = 250
        self.vel = 10
        self.ang = 0

    def Move(self):
        self.x += self.vel * math.cos(self.ang)
        self.y += self.vel * math.sin(self.ang)

    def Draw(self, window):
        window.blit(self.img, (self.x, self.y))

    def Collide(self, pad):
        pad_mask = pad.GetMask()
        self_mask = pygame.mask.from_surface(self.img)
        collide_point = pad_mask.overlap(self_mask, (round(self.x - pad.x), round(self.y - pad.y)))

        if collide_point:
            return True
        else:
            return False

    def Bounce(self, type, pad=0):

        bounceOffset = 7

        # pad collision
        if type == 0:
            if pad.x < 500:
                self.x += bounceOffset
                self.ang = 0.5*(random.random() - 0.1) + 0
            else:
                self.x -= bounceOffset
                self.ang = 0.5*(random.random() - 0.1) + 91
        # wall collision
        else:
            if self.y < 300:
                self.y += bounceOffset
            else:
                self.y -= bounceOffset
            self.ang = -self.ang
