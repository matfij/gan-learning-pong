from controllers import FileLoader
import pygame


class PadModel:

    def __init__(self, x, y):
        IMG_FILE = FileLoader.LoadFile("paddle.png", "img")
        self.img = pygame.image.load(IMG_FILE)
        self.x = x
        self.y = y
        self.vel = 10
        self.ang = 0

    def Move(self, direction=0):
        if direction == 1:
            self.y += self.vel
        elif direction == 2:
            self.y -= self.vel

    def GetMask(self):
        return pygame.mask.from_surface(self.img)

    def Draw(self, window):
        window.blit(self.img, (self.x, self.y))
